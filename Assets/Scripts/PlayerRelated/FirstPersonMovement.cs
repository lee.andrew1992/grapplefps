﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ConstantValues;
using Utility;

public class FirstPersonMovement : MonoBehaviour {
    public Camera cam;

    private CharacterController body;
    private bool isGrounded;
    private Transform groundChecker;
    private LayerMask defaultLayer = (1 << NumericalValues.Layers.Default);
    private Vector3 rawInputs = Vector3.zero;
    private Vector3 movement = Vector3.zero;
    private float gravity = 0;
    private Vector3 slideMovement = Vector3.zero;
    private Vector3 initialSlideMovement = Vector3.zero;
    private Vector3 initialSlideMovementMax = Vector3.zero;
    private float moveSpeed;
    private MoveState moveState;
    
    public MoveState currMoveState { get { return moveState; } }
    public CharacterController playerBody { get { return body; } }
    public Vector3 currMoveVector { get { return movement; } }
    public Vector3 currMoveInputs { get { return rawInputs; } }
    public bool onGround { get { return isGrounded; } }
    public bool applyGravity;

    public enum MoveState
    {
        Normal,
        Crouching,
        Running,
        Sliding,
        Grappling,
        Attached
    }

    // Use this for initialization
    void Start () {
        moveState = MoveState.Normal;
        body = this.GetComponent<CharacterController>();
        groundChecker = this.transform.GetChild(0);
        GameTracker.PlayerRelated.FirstPersonMovement = this;
        moveSpeed = NumericalValues.PlayerMovement.MovementSpeed;
        applyGravity = true;
    }

	// Update is called once per frame
	void Update () {
        isGrounded = Physics.CheckSphere(groundChecker.position, NumericalValues.PlayerMovement.GroundCheckDistance, defaultLayer, QueryTriggerInteraction.Ignore);

        float horizontal = Input.GetAxisRaw(StringValues.Input.Horizontal);
        float vertical = Input.GetAxisRaw(StringValues.Input.Vertical);

        rawInputs = new Vector3 (horizontal, 0, vertical);

        movement = CalculateMovementVector(rawInputs);

        // Modifiers
        if (isGrounded)
        {
            if (Input.GetButtonDown(StringValues.Input.CrouchToggle))
            {
                switch (moveState)
                {
                    case MoveState.Sliding:
                    case MoveState.Crouching:
                        moveState = MoveState.Normal;
                        break;

                    case MoveState.Running:
                        if (isGrounded)
                        {
                            initialSlideMovement = movement * (1 + NumericalValues.PlayerMovement.InitialSlideBonusValue);
                            initialSlideMovementMax = initialSlideMovement;
                            moveState = MoveState.Sliding;
                        }
                        else
                        {
                            moveState = MoveState.Crouching;
                        }
                        break;

                    case MoveState.Normal:
                        moveState = MoveState.Crouching;
                        break;
                }
            }
            if (Input.GetButtonDown(StringValues.Input.SprintToggle))
            {
                switch (moveState)
                {
                    case MoveState.Normal:
                    case MoveState.Crouching:
                        moveState = MoveState.Running;
                        break;

                    case MoveState.Running:
                        moveState = MoveState.Normal;
                        break;

                    default:
                        break;
                }
            }
        }
    }

    void FixedUpdate()
    {
        // Movement Part
        switch (moveState)
        {
            case MoveState.Running:
                cam.transform.localPosition = NumericalValues.PlayerCamera.OriginalPosition;
                moveSpeed = NumericalValues.PlayerMovement.MovementSpeed * (1 + NumericalValues.PlayerMovement.MovementRunBonusValue);
                if (movement == Vector3.zero)
                {
                    moveState = MoveState.Normal;
                }
                break;

            case MoveState.Crouching:
                cam.transform.localPosition = NumericalValues.PlayerCamera.CrouchPosition;
                moveSpeed = NumericalValues.PlayerMovement.MovementSpeed * NumericalValues.PlayerMovement.CrouchMovementReduction;
                break;

            case MoveState.Sliding:
                cam.transform.localPosition = NumericalValues.PlayerCamera.CrouchPosition;

                RaycastHit hit;
                if (Physics.Raycast(this.transform.position, -Vector3.up, out hit, NumericalValues.PlayerMovement.SlideAngleDetectionRange))
                {
                    Vector3 normal = hit.normal;
                    Vector3 crossDirection = Vector3.Cross(Vector3.up, normal);
                    initialSlideMovement = Vector3.Lerp(initialSlideMovement, Vector3.zero, Time.deltaTime * NumericalValues.PlayerMovement.SlideBoostDecreaseValue);
                    float initialSlideMovementPercentage = initialSlideMovement.magnitude / initialSlideMovementMax.magnitude;

                    slideMovement = Vector3.Lerp(slideMovement, (Vector3.Cross(crossDirection, normal) * NumericalValues.PlayerMovement.SlideAmplifier), Time.deltaTime);
                    slideMovement = slideMovement * (1 - initialSlideMovementPercentage) + initialSlideMovement;
                }
                break;

            case MoveState.Attached:
            case MoveState.Grappling:
                moveSpeed = 0;
                break;

            default:
                moveSpeed = NumericalValues.PlayerMovement.MovementSpeed;
                cam.transform.localPosition = NumericalValues.PlayerCamera.OriginalPosition;
                break;
        }

        if (moveState == MoveState.Sliding)
        {
            body.Move(slideMovement * moveSpeed * Time.fixedDeltaTime);
        }
        else
        {
            body.Move(movement * moveSpeed * Time.fixedDeltaTime);
        }

        //Gravity
        if (applyGravity)
        {
            gravity += -NumericalValues.Environment.Gravity * Time.fixedDeltaTime;
        }
        else
        {
            ResetGravity();
        }

        // Jumping Part
        if (isGrounded)
        {
            if (Input.GetButtonDown(StringValues.Input.Jump))
            {
                switch (moveState)
                {
                    case MoveState.Crouching:
                        moveState = MoveState.Normal;
                        break;

                    case MoveState.Sliding:
                        moveState = MoveState.Running;
                        break;

                    default:
                        break;
                }

                ResetGravity();
                gravity += Mathf.Sqrt(NumericalValues.PlayerMovement.JumpHeight * 2f * NumericalValues.Environment.Gravity);
            }
        }

        if (gravity < -NumericalValues.Environment.Gravity)
        {
            gravity = -NumericalValues.Environment.Gravity;
        }
        if (gravity > NumericalValues.PlayerMovement.JumpHeight)
        {
            gravity = NumericalValues.PlayerMovement.JumpHeight;
        }

        body.Move(new Vector3 (0, gravity, 0) * Time.fixedDeltaTime);
    }

    private Vector3 CalculateMovementVector(Vector3 rawInputs)
    {
        Vector3 forward = new Vector3(cam.transform.forward.x, 0, cam.transform.forward.z);
        Vector3 right = cam.transform.right;

        Vector3 movement = ((rawInputs.z * forward).normalized + (rawInputs.x * right).normalized);
        float movementSum = Mathf.Abs(movement.x) + Mathf.Abs(movement.z);
        movement = (transform.TransformDirection(movement) * NumericalValues.PlayerMovement.MovementSpeed);
        if (movementSum > 0)
        {
            movement = movement / Mathf.Sqrt(movementSum);
        }

        return new Vector3(movement.x, 0, movement.z);
    }

    public Vector3 CalculateHorizontalMovementOnly(float horizontalMovement) {
        return CalculateMovementVector(new Vector3(horizontalMovement, 0, 0));
    }

    public void SetMoveState(MoveState desiredMovement)
    {
        moveState = desiredMovement;
    }

    public void ResetGravity()
    {
        gravity = 0;
    }
}
