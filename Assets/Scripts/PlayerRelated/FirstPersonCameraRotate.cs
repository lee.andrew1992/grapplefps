﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ConstantValues;

public class FirstPersonCameraRotate : MonoBehaviour
{
    public float sensitivityX;
    public float sensitivityY;
    public float minimumY;
    public float maximumY;

    float rotationX = 0F;
    float rotationY = 0F;

    public Texture2D crosshairIcon;
    float xMin;
    float yMin;

    void Start()
    {
        GameTracker.PlayerRelated.FirstPersonCameraRotate = this;
        GameTracker.PlayerRelated.MainCam = this.GetComponent<Camera>();
        Cursor.lockState = CursorLockMode.Locked;
    }

    void Update()
    {
        rotationY = Input.GetAxisRaw(StringValues.Input.MouseY) * sensitivityY * 10 * Time.deltaTime;
        rotationX = Input.GetAxisRaw(StringValues.Input.MouseX) * sensitivityX * 10 * Time.deltaTime;

        Vector3 rotation = transform.rotation.eulerAngles + new Vector3(-rotationY, rotationX, 0f);
        rotation.x = ClampAngle(rotation.x, minimumY, maximumY);
        transform.eulerAngles = rotation;

        //if (GameTracker.PlayerRelated.FirstPersonMovement.isCrouched)
        //{
        //    this.transform.localPosition = NumericalValues.PlayerCamera.CrouchPosition;
        //}
        //else
        //{
        //    this.transform.localPosition = NumericalValues.PlayerCamera.OriginalPosition;
        //}

    }

    void OnGUI()
    {
        xMin = (Screen.width / 2) - (crosshairIcon.width / 2);
        yMin = (Screen.height / 2) - (crosshairIcon.height / 2);
        GUI.DrawTexture(new Rect(xMin, yMin, crosshairIcon.width, crosshairIcon.height), crosshairIcon);
    }

    float ClampAngle(float angle, float from, float to)
    {
        if (angle < 0f)
        {
            angle = 360 + angle;
        }
        if (angle > 180f)
        {
            return Mathf.Max(angle, 360 + from);
        }
        return Mathf.Min(angle, to);
    }
}