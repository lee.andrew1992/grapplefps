﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ConstantValues;

public class Inventory : MonoBehaviour {

    public int currentInventoryIndex = 0;
    public Weapon[] weaponSlots;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetButton(StringValues.Input.Inventory1))
        {
            currentInventoryIndex = 0;
        }

        if (Input.GetButton(StringValues.Input.Inventory2))
        {
            currentInventoryIndex = 1;
        }

        if (Input.GetButton(StringValues.Input.Inventory3))
        {
            currentInventoryIndex = 2;
        }


    }
}
