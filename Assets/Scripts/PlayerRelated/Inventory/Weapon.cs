﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ConstantValues;

public class Weapon : MonoBehaviour {

    public int totalBullets;
    public int clipSize;
    public int currentBullets;
    public float reloadTime;
    public float damage;
    public float fireRate;
    public float range;
    public float recoil;
    public float screenShake;
    public Transform bulletHole;
    public ParticleSystem hitSmoke;
    public ParticleSystem hitSparks;
    public Vector3 bulletHoleSizeMin;
    public Vector3 bulletHoleSizeMax;
    public WeaponValues.WeaponType weaponType;

    protected bool reloading;
    protected float timeToFire;

    private const float SHAKE_DURATION = 0.15f;

    void Start()
    {
        timeToFire = Time.time;
        reloading = false;
    }

    public virtual void Fire(Transform camera)
    {
        if (currentBullets > 0 && Time.time >= timeToFire && !reloading)
        {
            currentBullets--;
            StartCoroutine(ShakeCam(camera, screenShake, SHAKE_DURATION));
            RaycastHit hit;

            if (Physics.Raycast(camera.transform.position, camera.forward, out hit, Mathf.Infinity))
            {
                if (hit.collider.gameObject.layer == NumericalValues.Layers.Default || hit.collider.gameObject.layer == NumericalValues.Layers.Decal)
                {
                    // Create bullet hole at hit location
                    StartCoroutine(ApplyParticleEffect(hitSmoke, hit));
                    StartCoroutine(ApplyParticleEffect(hitSparks, hit));
                    CreateBulletHole(hit);
                }
                else if (hit.collider.gameObject.layer == NumericalValues.Layers.Enemy)
                {
                    // Create blood spurt
                }
            }
            timeToFire = Time.time + 1 / fireRate;
        }
    }

    public virtual IEnumerator Reload()
    {
        reloading = true;
        int remainingBullets = currentBullets;
        yield return new WaitForSeconds(reloadTime);
        currentBullets = clipSize;
        totalBullets -= (clipSize - remainingBullets);
        reloading = false;
    }

    private IEnumerator ShakeCam(Transform camera, float magnitude, float duration)
    {
        float elapsed = 0;
        //StopAllCoroutines();
        while (elapsed < duration)
        {
            Vector3 shakeValues = Vector3.zero;
            // Reduce X shake by half relative to y shake
            if (GameTracker.PlayerRelated.FirstPersonMovement.currMoveState == FirstPersonMovement.MoveState.Crouching || 
                GameTracker.PlayerRelated.FirstPersonMovement.currMoveState == FirstPersonMovement.MoveState.Sliding)
            {
                shakeValues = (Random.insideUnitSphere * magnitude) * WeaponValues.Common.recoilCrouchReduction;
                camera.transform.localPosition = (new Vector3(shakeValues.x + NumericalValues.PlayerCamera.CrouchPosition.x, shakeValues.y + NumericalValues.PlayerCamera.CrouchPosition.y, shakeValues.z + NumericalValues.PlayerCamera.CrouchPosition.z));
            }
            else
            {
                shakeValues = (Random.insideUnitSphere * magnitude);
                camera.transform.localPosition = (new Vector3 (shakeValues.x + NumericalValues.PlayerCamera.OriginalPosition.x, shakeValues.y + NumericalValues.PlayerCamera.OriginalPosition.y, shakeValues.z + NumericalValues.PlayerCamera.OriginalPosition.z ));
            }

            elapsed += Time.deltaTime;
            yield return null;
        }

        switch(GameTracker.PlayerRelated.FirstPersonMovement.currMoveState)
        {
            case FirstPersonMovement.MoveState.Sliding:
            case FirstPersonMovement.MoveState.Crouching:
                camera.transform.localPosition = NumericalValues.PlayerCamera.CrouchPosition;
                break;

            default:
                camera.transform.localPosition = NumericalValues.PlayerCamera.OriginalPosition;
                break;
        }
    }

    private IEnumerator ApplyParticleEffect(ParticleSystem effect, RaycastHit hit)
    {
        Transform hitObject = hit.transform;

        if (hitObject.gameObject.layer == NumericalValues.Layers.Decal)
        {
            hitObject = hit.transform.parent;
        }

        ParticleSystem ps = Instantiate(effect, hit.point, Quaternion.identity, hitObject);
        ps.transform.rotation = Quaternion.LookRotation(hit.normal);
        ps.gameObject.SetActive(true);
        yield return new WaitForSeconds(ps.main.duration);
        Destroy(ps.gameObject);
    }

    private void CreateBulletHole(RaycastHit hit)
    {
        Debug.Log("create hole");

        Transform hitObject = hit.transform;

        if (hitObject.gameObject.layer == NumericalValues.Layers.Decal)
        {
            hitObject = hit.transform.parent;
        }
        
        Transform hole = Instantiate(bulletHole, hit.point, Quaternion.LookRotation(-hit.normal));
        hole.localScale = new Vector3(Random.Range(bulletHoleSizeMin.x, bulletHoleSizeMax.x), Random.Range(bulletHoleSizeMin.y, bulletHoleSizeMax.y), Random.Range(bulletHoleSizeMin.z, bulletHoleSizeMax.z));

        hole.transform.parent = hitObject;
        hole.position = hole.position - (hole.forward * 0.001f);
    }
}
