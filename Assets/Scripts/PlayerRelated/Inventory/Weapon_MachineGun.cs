﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ConstantValues;

public class Weapon_MachineGun : Weapon {

    void Start()
    {
        totalBullets = WeaponValues.MachineGun.totalBullets;
        clipSize = WeaponValues.MachineGun.clipSize;
        currentBullets = this.clipSize;
        reloadTime = WeaponValues.MachineGun.reloadTime;
        damage = WeaponValues.MachineGun.damage;
        fireRate = WeaponValues.MachineGun.fireRate;
        range = WeaponValues.MachineGun.range;
        recoil = WeaponValues.MachineGun.recoil;
        screenShake = WeaponValues.MachineGun.screenShake;
        weaponType = WeaponValues.MachineGun.weaponType;
        bulletHoleSizeMin = WeaponValues.MachineGun.bulletHoleSizeMin;
        bulletHoleSizeMax = WeaponValues.MachineGun.bulletHoleSizeMax;
    }
}
