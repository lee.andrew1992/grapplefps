﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ConstantValues;

public class FireGrappleHook : MonoBehaviour {

    public GrappleHook hook;
    public Transform origin;

    public float hookSpeed;
    public float hookGravityDelay;
    public float maxRange;

    private Camera mainCam;

    // Use this for initialization
    void Start () {
        GameTracker.PlayerRelated.FireGrappleHook = this;
        mainCam = GameTracker.PlayerRelated.MainCam;
    }
	
	// Update is called once per frame
	void Update () {
        if (hook.armed)
        {
            if (Input.GetButtonDown(StringValues.Input.FireHook))
            {
                StopAllCoroutines();
                hook.FireHook(transform.forward.normalized, hookSpeed);
                StartCoroutine(hook.DelayedGravityOn(hookGravityDelay));
            }
        }
        else
        {
            Vector3 heading = mainCam.transform.position - hook.transform.position;
            bool behindGrappleSpot = Vector3.Dot(heading, hook.transform.forward) < 0;

            //if (Input.GetButtonDown(StringValues.Input.FireHook) || Input.GetButtonDown(StringValues.Input.Crouch) || Input.GetButtonDown(StringValues.Input.CrouchToggle) || Vector3.Distance(this.transform.position, hook.transform.position) > maxRange || (hook.stuck && behindGrappleSpot))
            if (Input.GetButtonDown(StringValues.Input.FireHook) || Vector3.Distance(this.transform.position, hook.transform.position) > maxRange)
            {
                StopAllCoroutines();
                StartCoroutine(hook.RetractHook());
            }
        }
    }
}
