﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ConstantValues;

public class GrappleHook : MonoBehaviour {

    bool readyToFire = true;
    private GameObject hookTarget;
    private Transform originalParent;
    private Vector3 stuckPoint;
    private Vector3 grappleStartPoint;
    private Vector3 grappleStopPoint;
    private Vector3 pullVector = Vector3.zero;

    public Rigidbody rb;
    public Collider collider;
    public FirstPersonMovement playerMovement;
    public Transform grappleOrigin;
    public bool stuck;
    public bool armed { get { return readyToFire; } }

    // Use this for initialization
    void Start () {
        originalParent = this.transform.parent;
        GameTracker.PlayerRelated.GrappleHook = this;
        rb = this.GetComponent<Rigidbody>();
        playerMovement = GameTracker.PlayerRelated.FirstPersonMovement;
        //player = playerMovement.gameObject;
        collider = this.GetComponent<Collider>();
        collider.enabled = false;
        stuck = false;
    }
	
	// Update is called once per frame
	void Update () {
        if (readyToFire)
        {
            this.transform.position = grappleOrigin.position;
            this.transform.rotation = Quaternion.Euler(Vector3.zero);
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer != NumericalValues.Layers.LocalPlayer)
        {
            Stick(collision.gameObject);
        }
    }

    public IEnumerator DelayedGravityOn(float delay)
    {
        readyToFire = false;
        yield return new WaitForSeconds(delay);
        if (!stuck)
        {
            rb.useGravity = true;
        }
    }

    public void Reset()
    {
        this.transform.parent = originalParent;
        rb.useGravity = false;
        rb.constraints = RigidbodyConstraints.FreezePosition;
        rb.constraints = RigidbodyConstraints.FreezeRotation;
        readyToFire = true;

        hookTarget = null;
        stuck = false;
        this.GetComponent<Renderer>().enabled = false;
    }

    public void Stick(GameObject collidedObject)
    {
        rb.useGravity = false;
        rb.constraints = RigidbodyConstraints.FreezePosition;
        rb.constraints = RigidbodyConstraints.FreezeRotation;
        this.collider.enabled = false;
        hookTarget = collidedObject;
        //this.transform.LookAt(new Vector3(playerMovement.transform.position.x, this.transform.position.y, playerMovement.transform.position.z));
        stuck = true;
        SetStuckPoint(collidedObject.transform);
        StartCoroutine(PullPlayerTowardsPoint(playerMovement.currMoveInputs));
    }

    public void FireHook(Vector3 direction, float hookSpeed)
    {
        Reset();
        pullVector = Vector3.zero;
        collider.enabled = true;
        this.GetComponent<Renderer>().enabled = true;
        rb.constraints = RigidbodyConstraints.None;
        rb.velocity = direction.normalized * hookSpeed;

        readyToFire = false;
    }

    public IEnumerator PullPlayerTowardsPoint(Vector3 rawInputs)
    {
        grappleStartPoint = playerMovement.transform.position;
        Vector3 rawInputsSnapShot = rawInputs;
        Vector3 horizontalOnlyMovement = playerMovement.CalculateHorizontalMovementOnly(rawInputs.x);
        playerMovement.SetMoveState(FirstPersonMovement.MoveState.Grappling);

        while (Vector3.Distance(playerMovement.transform.position, stuckPoint) > 0.5f && stuck)
        {
            playerMovement.applyGravity = false;
            float pullSpeed = Mathf.Sqrt(NumericalValues.Grapple.PullSpeed * NumericalValues.Environment.Gravity);
            pullVector = (stuckPoint - playerMovement.transform.position + horizontalOnlyMovement) * Time.fixedDeltaTime * pullSpeed;
            yield return null;
        }

        pullVector = Vector3.zero;
        playerMovement.SetMoveState(FirstPersonMovement.MoveState.Attached);
        //Reset();
    }

    public IEnumerator RetractHook()
    {
        StopAllCoroutines();

        grappleStopPoint = playerMovement.transform.position;
        playerMovement.SetMoveState(FirstPersonMovement.MoveState.Normal);
        playerMovement.applyGravity = true;
        this.collider.enabled = false;
        rb.velocity = Vector3.zero;

        while (Vector3.Distance(this.transform.position, playerMovement.transform.position) > 0.5f)
        {
            this.transform.position = Vector3.MoveTowards(this.transform.position, playerMovement.transform.position, NumericalValues.Grapple.RetractSpeed * Time.fixedDeltaTime);
            yield return null;
        }

        Reset();
    }

    public void SetStuckPoint(Transform newParent)
    {
        stuckPoint = this.transform.position;
        this.transform.parent = newParent;
    }

    void FixedUpdate()
    {
        switch (playerMovement.currMoveState)
        {
            case FirstPersonMovement.MoveState.Grappling:
                playerMovement.playerBody.Move(pullVector);
                break;
            case FirstPersonMovement.MoveState.Normal:
                if (playerMovement.onGround)
                {
                    pullVector = Vector3.zero;
                }
                else
                {
                    float fullDist = Vector3.Distance(stuckPoint, grappleStartPoint);
                    float travelledDist = Vector3.Distance(grappleStopPoint, grappleStopPoint);

                    pullVector = Vector3.Lerp(pullVector, Vector3.zero, Time.fixedDeltaTime * (2 - (travelledDist / fullDist)));
                    playerMovement.playerBody.Move(pullVector);
                }
                break;
        }
    }
}
