﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ConstantValues;

public class FirstPersonShooting : MonoBehaviour {

    public Weapon currentWeapon;
    Inventory myInventory;

	// Use this for initialization
	void Start () {
        myInventory = this.transform.parent.GetComponent<Inventory>();
    }
	
	// Update is called once per frame
	void Update () {
        currentWeapon = myInventory.weaponSlots[myInventory.currentInventoryIndex];

        if (!Input.GetButton(StringValues.Input.LeftAlt))
        {
            if (currentWeapon.weaponType == WeaponValues.WeaponType.Automatic)
            {
                if (Input.GetButton(StringValues.Input.LeftMouseClick))
                {
                    currentWeapon.Fire(this.transform);
                }
            }
            else
            {
                if (Input.GetButtonDown(StringValues.Input.LeftMouseClick))
                {
                    currentWeapon.Fire(this.transform);
                }
            }
        }

        if (Input.GetButtonDown(StringValues.Input.Reload))
        {
            StartCoroutine(currentWeapon.Reload());
        }

        if (Input.GetButtonDown(StringValues.Input.Inventory1))
        {
            if (myInventory.currentInventoryIndex == 0)
            {
                //Do nothing
            }

            if (myInventory.currentInventoryIndex == 1)
            {
                StopAllCoroutines();
                myInventory.currentInventoryIndex = 0;
            }

            if (myInventory.currentInventoryIndex == 1)
            {
                //Do nothing
            }

            if (myInventory.currentInventoryIndex == 0)
            {
                StopAllCoroutines();
                myInventory.currentInventoryIndex = 1;
            }
        }
	}
}
