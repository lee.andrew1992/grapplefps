﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ConstantValues;

public class GameTracker : MonoBehaviour {

    public static class PlayerRelated
    {
        public static FirstPersonMovement FirstPersonMovement;
        public static GrappleHook GrappleHook;
        public static FireGrappleHook FireGrappleHook;
        public static FirstPersonCameraRotate FirstPersonCameraRotate;
        public static Camera MainCam;
    }

    void Awake()
    {
        Physics.gravity = new Vector3 (0, -NumericalValues.Environment.Gravity, 0);
    }
}
