﻿using System.Collections;
using System.Collections.Generic;
using ConstantValues;
using UnityEngine;

public class AssetLoader : MonoBehaviour {

    public Maps mapSelection;
    public List<GameObject> maps;
    public GameObject playerObject;

    public enum Maps
    {
        Demo = 0,
        GaiaDemo = 1
    }

    private GameObject currentMap = null;

	// Use this for initialization
	void Start () {
		
	}

    void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
        Queue<GameObject> assets = new Queue<GameObject>();
        assets.Enqueue(maps[(int)mapSelection]);
        assets.Enqueue(playerObject);

        StartCoroutine(Loader(assets));
    }

    IEnumerator Loader(Queue <GameObject> assets)
    {
        GameObject asset = assets.Dequeue();

        if (asset.layer == NumericalValues.Layers.Map)
        {
            currentMap = asset;
            Instantiate(asset);
        }
        else if (asset.layer == NumericalValues.Layers.LocalPlayer)
        {
            Vector3 spawnLocation = Vector3.zero;
            foreach (Transform child in currentMap.transform)
            {
                if (child.gameObject.layer == NumericalValues.Layers.PlayerSpawn)
                {
                    spawnLocation = child.transform.position;
                }
            }
            Instantiate(asset, spawnLocation, Quaternion.identity);
        }
        if (assets.Count > 0)
        {
            yield return new WaitForSeconds(0.5f);
            yield return StartCoroutine(Loader(assets));
        }
        yield return null;
    }
}
