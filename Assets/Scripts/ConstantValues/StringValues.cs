﻿namespace ConstantValues
{
    public static class StringValues
    {
        public struct Input
        {
            public static string Horizontal = "Horizontal";
            public static string Vertical = "Vertical";
            public static string Jump = "Jump";
            public static string LeftMouseClick = "Fire1";
            public static string RightMouseClick = "Fire2";
            public static string MouseX = "Mouse X";
            public static string MouseY = "Mouse Y";
            public static string LeftAlt = "LAlt";
            public static string Inventory1 = "Inventory1";
            public static string Inventory2 = "Inventory2";
            public static string Inventory3 = "Inventory3";
            public static string Reload = "Reload";
            public static string SprintToggle = "SprintToggle";
            public static string Crouch = "Crouch";
            public static string CrouchToggle = "CrouchToggle";
            public static string FireHook = "FireHook";
        }
    }
}
