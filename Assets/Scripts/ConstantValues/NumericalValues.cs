﻿using UnityEngine;

namespace ConstantValues
{
    public static class NumericalValues
    {
        public struct Layers
        {
            public static int Default = 0;
            public static int Map = 16;
            public static int PlayerSpawn = 17;
            public static int Decal = 18;
            public static int Enemy = 19;
            public static int LocalPlayer = 30;
        }

        public struct Environment
        {
            public static float Gravity = 20f;
        }

        public struct PlayerMovement
        {
            public static float JumpValue = 50f;
            public static float JumpHeight = 7;
            public static float InitialJumpSpeed = 5;
            public static float MovementSpeed = 2.5f;
            public static float MovementRunBonusValue = 0.4f;
            public static float InitialSlideBonusValue = 0.40f;
            public static float SlideBoostDecreaseValue = 8f;
            public static float CrouchMovementReduction = 0.6f;
            public static float SlideableWindow = 0.75f;
            public static float SlideAngleDetectionRange = 4;
            public static float SlideAmplifier = 10;
            public static float GroundCheckDistance = 0.35f;
        }

        public struct Grapple
        {
            public static float PullSpeed = 1;
            public static float RetractSpeed = 30;
            public static float PlayerCamDirectionFactorIntensity = 5;
            public static float PullSpeedBuildUpBase = 0.5f;
        }

        public struct PlayerCamera
        {
            public static Vector3 OriginalPosition = new Vector3(0, 0.7f, 0);
            public static Vector3 CrouchPosition = new Vector3(0, 0.45f, 0);
        }
    }
}
