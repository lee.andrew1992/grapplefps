﻿using UnityEngine;

namespace ConstantValues
{
    public static class WeaponValues
    {
        public enum WeaponType
        {
            Automatic,
            Manual
        }

        public struct Common
        {
            public static float recoilCrouchReduction = 0.5f;
        }

        public struct MachineGun
        {
            public static int totalBullets = 180;
            public static int clipSize = 30;
            public static float reloadTime = 1.25f;
            public static float damage = 11f;
            public static float fireRate = 8f;
            public static float range = 50f;
            public static float recoil = 1;
            public static float screenShake = 0.15f;
            public static WeaponType weaponType = WeaponType.Automatic;
            public static Vector3 bulletHoleSizeMin = new Vector3(0.05f, 0.05f, 0.05f);
            public static Vector3 bulletHoleSizeMax = new Vector3(0.1f, 0.1f, 0.1f);
        }
    }
}
