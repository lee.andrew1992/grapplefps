﻿using UnityEngine;

namespace Utility
{
    public static class UtilityFunctions {

        public static bool SameSign(float num1, float num2)
        {
            if (Mathf.Abs(num1) < 0.05f || Mathf.Abs(num2) < 0.05f)
            {
                return false;
            }

            if ((num1 > 0 && num2 > 0) || (num1 < 0 && num2 < 0))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool WithinView(Camera camera, Transform targetObject)
        {
            Vector3 withinView = camera.WorldToViewportPoint(targetObject.transform.position);
            return (BetweenTwoValues(0, 1, withinView.x) && BetweenTwoValues(0, 1, withinView.y));
        }

        public static bool BetweenTwoValues(float min, float max, float value)
        {
            return (value >= min && value <= max);
        }
    }
}

